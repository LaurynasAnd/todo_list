const addTodoForm = document.querySelector('.add-todo-form');
addTodoForm.addEventListener('submit', handleFormSubmit);
const completeButtons = document.querySelectorAll('.button-complete');
const deleteButtons = document.querySelectorAll('.button-delete');
for (const completeButton of completeButtons){
    completeButton.addEventListener('click', taskComplete);
}
for (const deleteButton of deleteButtons){
    deleteButton.addEventListener('click', deleteTask);
}

function handleFormSubmit(e){
    e.preventDefault();
    const todoList = document.querySelector('.todo-list');
    const inputNewTodo = document.querySelector('input');

    const newTodoItem = document.createElement('li'); //create list item
    newTodoItem.classList.add('todo-item');
    newTodoItem.textContent = inputNewTodo.value;
    todoList.appendChild(newTodoItem);                                      //insert list item into list

    const buttonDiv = document.createElement('div');                    //create div which will contain buttons
    buttonDiv.classList.add('buttons');
    newTodoItem.appendChild(buttonDiv);

    const newCompleteButton = document.createElement('button');         //create Complete button
    newCompleteButton.classList.add('button-complete');
    newCompleteButton.textContent = 'Complete';
    buttonDiv.appendChild(newCompleteButton);

    const newDeleteButton = document.createElement('button');           //create Delete button
    newDeleteButton.classList.add('button-delete');
    newDeleteButton.textContent = 'Delete';
    buttonDiv.appendChild(newDeleteButton);

    inputNewTodo.value = null;

    newCompleteButton.addEventListener('click', taskComplete);          //add event listeners for new buttons
    newDeleteButton.addEventListener('click', deleteTask);
}

function taskComplete(e){
    e.target.textContent = (e.target.textContent == 'Complete') ? 'Revert' : 'Complete';
    e.target.closest('.todo-item').classList.toggle('bg-gray');
}
function deleteTask(e){
    e.target.closest('.todo-item').remove();
}